<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pays')->insertOrIgnore([
            ['nom' => 'Togo','indicatif'=>'+228'],
        ]);
    }
}
