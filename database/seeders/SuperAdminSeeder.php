<?php

namespace Database\Seeders;

use App\Models\Administrateur;
use App\Models\Administrator;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin = Administrateur::create(["nom_societe"=> "E-Solux"]);
        $administrator = User::create([
            'nom'=>'e-solux',
            'prenoms'=>'e-solux',
            'users_type_type'=>"App\Models\Administrateur",
            'users_type_id'=> $admin["id"],
            'password'=>bcrypt('administrateur'),
            'telephone'=>"22870517794",
        ]);

        $role1 = Role::find(1);
        $administrator->assignRole($role1);
    }
}
