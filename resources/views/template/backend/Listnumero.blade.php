@extends('template.template')

@section('css')
    <link rel="stylesheet" href=" {{asset('assets/css/lib/datatable/dataTables.bootstrap.min.css')}} ">
@endsection


@section('contenu')
<div class="animated fadeIn">
    <!-- Widgets  -->

    <div class="row">
        <div class="col-lg-3 col-md-6">
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib">
                            <i class="ti-user text-primary border-primary"></i>
                        </div>
                        <div class="stat-content dib">
                            <div class="text-left dib">
                                <div class="stat-text"><span class="count">{{count($number)}}</span></div>
                                <div class="stat-heading">Nombre de numero</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>






    </div>
    <!-- /Widgets -->
    <!--  Traffic  -->

    <!--  /Traffic -->
    <div class="clearfix"></div>
    <!-- Orders -->
    <div class="orders">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="box-title" >Liste des numeros
                            <a class="btn btn-primary"  href="#ModiferModal" data-toggle="modal" >Ajouter un numero</a>
                        </h4>
                        <!-- Modal -->
                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="ModiferModal" class="modal fade">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">

                                  <h5 class="modal-title">Ajouter un numero</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                            <form method="post" action="{{route('storenumber')}}" >
                                   @csrf
                                <div class="modal-body">

                                    <div class="col-lg-12">

                                    </div>
                                        <label for="ccomment" class="control-label col-lg-5">Numero</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="cname" name="number" minlength="2" type="text" required />
                                        </div>



                                        <label for="cname" class="control-label col-lg-5">Operateur</label>
                                        <div class="col-lg-10">
                                            <select data-placeholder="Choose a Country..." class="standardSelect form-control" name="operateur" tabindex="1">
                                                <option value="" label="default" disabled></option>
                                                @foreach ($operateur as $operateur )
                                                    <option value="{{$operateur["id"]}}">{{$operateur["nom"]}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="ccomment" class="control-label col-lg-8">Solde</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="cnamess" name="solde" minlength="2" type="text" required />
                                        </div>

                                </div>
                                <div class="modal-footer">
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-6 em">
                                            <button class="btn btn-primary" type="submit">Valider</button>
                                        </div>

                                      </div>
                                </div>
                                </form>
                              </div>
                            </div>
                        </div>



                    </div>
                    <div class="card-body--">
                        <div class="table-stats order-table ov-h">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Numero</th>
                                        <th>Operateur</th>
                                        <th>Somme </th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($number as $number)
                                    <tr>
                                        <td>  <span class="name">{{$number["number"]}}</span> </td>
                                        <td> <span class="product">{{$number["operateur"]["nom"]}}</span> </td>
                                        <td> <span class="count">{{$number["solde"]}}</span> </td>
                                        <td> </td>




                                    </tr>
                                    @endforeach






                                </tbody>
                            </table>
                        </div> <!-- /.table-stats -->
                    </div>
                </div> <!-- /.card -->
            </div>  <!-- /.col-lg-8 -->

             <!-- /.col-md-4 -->
        </div>
    </div>
    <!-- /.orders -->
    <!-- To Do and Live Chat -->

    <!-- /To Do and Live Chat -->
    <!-- Calender Chart Weather  -->

    <!-- /Calender Chart Weather -->
    <!-- Modal - Calendar - Add New Event -->

    <!-- /#event-modal -->
    <!-- Modal - Calendar - Add Category -->

<!-- /#add-category -->
</div>
@section('js')
<script src="{{asset('assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{asset('assets/js/init/datatables-init.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function() {
      $('#bootstrap-data-table-export').DataTable();
  } );
</script>
<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Aucun resultat trouve!",
            width: "100%"
        });
    });
</script>

@endsection
@endsection
