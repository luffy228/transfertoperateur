<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{route('home')}}"><i class="menu-icon fa fa-laptop"></i>Home </a>
                </li>
                @hasrole("Administrateur")
                <li class="menu-title">Transactions</li><!-- /.menu-title -->
                    <li>
                        <a href="#"> <i class="menu-icon ti-email"></i>Non gerer </a>
                    </li>
                    <li>
                        <a href="#"> <i class="menu-icon ti-email"></i>Recapitulatifs</a>
                    </li>
                    <li>
                        <a href="{{route('numeroall')}}"> <i class="menu-icon ti-email"></i>Liste Numeros</a>
                    </li>
                    <li>
                        <a href="{{route('virementsociete')}}"> <i class="menu-icon ti-email"></i>Virement E-Solux</a>
                    </li>
                <li class="menu-title">Administrations</li><!-- /.menu-title -->

                    <li>
                        <a href="{{route('getprofile')}}"> <i class="menu-icon ti-email"></i>Parametre compte</a>
                    </li>

                </li>

                <li >
                    <a href="{{route('logout')}}"><i class="menu-icon fa fa-laptop"></i>Deconnexion</a>
                </li>
            @endhasrole

            @hasrole("Secretaire")
                <li class="menu-title">Transactions</li><!-- /.menu-title -->
                    <li>
                        <a href="#"> <i class="menu-icon ti-email"></i>Non gerer </a>
                    </li>
                    <li>
                        <a href="#"> <i class="menu-icon ti-email"></i>Recapitulatifs</a>
                    </li>

                <li class="menu-title">Administrations</li><!-- /.menu-title -->

                    <li>
                        <a href="{{route('getprofile')}}"> <i class="menu-icon ti-email"></i>Parametre compte</a>
                    </li>

                </li>

                <li >
                    <a href="{{route('logout')}}"><i class="menu-icon fa fa-laptop"></i>Deconnexion</a>
                </li>
            @endhasrole

            @hasrole('SuperAdministrateur')

            <li class="menu-title">Administrations</li><!-- /.menu-title -->


                <li>
                    <a href="{{route('societeall')}}"> <i class="menu-icon ti-email"></i>Liste Societe</a>
                </li>

                <li>
                    <a href="{{route('secretaireall')}}"> <i class="menu-icon ti-email"></i>Liste Secretaire</a>
                </li>

                <li>
                    <a href="{{route('virementesolux')}}"> <i class="menu-icon ti-email"></i>Liste Virement</a>
                </li>

                <li>
                    <a href="{{route('virementesoluxwaiting')}}"> <i class="menu-icon ti-email"></i>Virement Attentes</a>
                </li>

                <li>
                    <a href="{{route('getprofile')}}"> <i class="menu-icon ti-email"></i>Parametre compte</a>
                </li>

            </li>

            <li >
                <a href="{{route('logout')}}"><i class="menu-icon fa fa-laptop"></i>Deconnexion</a>
            </li>

            @endhasrole




            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>
