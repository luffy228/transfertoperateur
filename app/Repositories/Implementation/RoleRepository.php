<?php

namespace  App\Repositories\Implementation;

use App\Models\User;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleRepository extends GenericRepository
{

    use ApiResponser;
    public function model()
    {
        return 'Spatie\Permission\Models\Role';
    }

    public function getAllRoles(){
        $roles =Role::all();
        return $this->successResponse($roles, 'user logged successfully', 200);
    }

    public function add(Request $request)
    {

        $formRequest = ['name' => $request["role"]];
        $this->getModel()->create($formRequest);
        return $this->successResponse(null, "Role add successfully", 200);
    }

    public function revokePermissionTo(Role $role, string $permissions) {
        $role->revokePermissionTo($permissions);
    }

    public function givePermissionTo(Role $role, array $permissions) {
        $role->givePermissionTo($permissions);
    }

    public function hasRole(User $user, $roles = []) {
        return $user->hasRole($roles);
    }

    public function hasAnyRole(User $user, $roles = []) {
        return $user->hasAnyRole($roles);
    }

    public function hasAllRoles(User $user, $roles = []) {
        return $user->hasAllRoles($roles);
    }
    public function getRoles(User $user){
        return $user->getRoleNames();
    }


}
