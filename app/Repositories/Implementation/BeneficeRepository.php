<?php
namespace App\Repositories\Implementation;
use App\Traits\ApiResponser;
use App\Http\Requests\administratorRequest;
use App\Models\Administrateur;
use App\Models\Administrator;
use App\Models\User;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use Illuminate\Http\Request;

class BeneficeRepository extends GenericRepository
{
    use ApiResponser;

    public function model()
    {
        return 'App\Models\Benefice';
    }

    public function adding(String $commission , String $admin_id)
    {
        $formRequest = [
            'commission'=> $commission,
            'administrateur_id'=> $admin_id,
            'statut' => "attente",

        ];
        return $this->getModel()->create($formRequest);
    }

    public function getBenefice(String $admin )
    {
        $record = $this->getModel()->where('administrateur_id', $admin)
                                    ->where('statut', "attente")
                                    ->first();
        return $record;

    }









}
