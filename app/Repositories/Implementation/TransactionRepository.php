<?php

namespace App\Repositories\Implementation;

use App\Traits\ApiResponser;
use App\Models\Transaction;
use App\Models\User;
use Ably\AblyRest;
use App\Models\Operateur;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ExternalApi;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionRepository extends GenericRepository
{
    use ApiResponser;
    use ExternalApi;


    public function model()
    {
        return 'App\Models\Transaction';
    }

    //public function adding(Request $request , User $user)
    public function adding(Request $request, Operateur $operateur , User $user)
    {

        $formRequest = [
            'client_id'=> $user["users_type_id"],
            'client_number' => $request["client_number"],
            'expediteur_number' => $request["expediteur_number"],
            'destinataire_number' => $request["destinataire_number_full"],
            'entreprise_number' => $request["entreprise_number"],
            'administrateur_id' => $request["administrateur"],
            'commission' => $request["frais"],
            'operateur_expediteur' => $request["operateur_expediteur"],
            'operateur_destinataire' => $request["operateur_destinataire"],
            'montant' => $request["montant"],
            'statut' => "Attente"
        ];
        $transaction = $this->getModel()->create($formRequest);
        try {
            $ably_send =
                [
                    'destinataire_number' => $request["destinataire_number"],
                    'entreprise_number' => $request["entreprise_number"],
                    'operateur_destinataire' => $request["operateur_destinataire"],
                    'montant' => $request["montant"],
                    'codeussd' => $operateur["ussd_code"],
                    'transaction' => $transaction["id"],
                ];


            $ably = new AblyRest($this->ablyToken());
            $ably->time();
            sleep(5);
            $ably->channel($this->ablyChannel())->publish('', $ably_send);


        } catch (Exception $e) {
            $updatetransaction = DB::table('transactions')
                ->where('id', $transaction["id"])
                ->update(array('statut' => "Echec"));
        }
        return $transaction;
    }


    public function updateStatut(Request $request)
    {
        $transaction = Transaction::find($request["transaction"]);
        $transaction->statut = $request["statut"];
        $transaction->save();
        return $transaction;

        //return $this->getModel()->update($formRequest);
    }

    public function historyClient(User $user)
    {
        if ($user["users_type_type"] == "App\Models\Client") {
            $transaction = Transaction::where('client_number', $user["telephone"])
            ->orwhere('destinataire_number', $user["telephone"])
                ->latest(DB::raw("DATE_FORMAT(transactions.created_at, '%m-%Y-%d %H:%i:%s')"))
                ->limit(10)
                ->select('client_number','expediteur_number','destinataire_number','statut','montant',DB::raw("(DATE_FORMAT(transactions.created_at, '%d-%m %H:%i:%s')) as mois"))
                ->get();



            return $transaction;
        } else {
            return null;
        }



        //return $this->getModel()->update($formRequest);
    }
    public function countClientTransaction($debut , $fin , User $user)
    {
        if ($user["users_type_type"] == "App\Models\Client") {
            $transaction = Transaction::where('client_id', $user["users_type_id"])
                ->whereBetween('created_at',[$debut,$fin])
                ->count();



            return $transaction;
        } else {
            return null;
        }

    }

    public function historyAdministrateur(User $user)
    {
        if ($user["users_type_type"] == "App\Models\Administrateur") {
            $transaction = Transaction::where('administrateur_id', $user["users_type_id"])
                ->where('statut', "Success")
                ->latest(DB::raw("DATE_FORMAT(transactions.created_at, '%m-%Y-%d %H:%i:%s')"))
                ->limit(5)
                ->select('destinataire_number', 'commission',DB::raw("(DATE_FORMAT(transactions.created_at, '%d-%m %H:%i:%s')) as mois"))
                ->get();
            return $transaction;
        } else {
            return null;
        }
        //return $this->getModel()->update($formRequest);
    }

    public function historyAdministrateurEchec(User $user)
    {
        if ($user["users_type_type"] == "App\Models\Administrateur") {
            $transaction = Transaction::where('administrateur_id', $user["users_type_id"])
                ->where('statut', "Echec")
                ->latest(DB::raw("DATE_FORMAT(transactions.created_at, '%m-%Y-%d %H:%i:%s')"))
                ->limit(20)
                ->select("id",'destinataire_number',"operateur_destinataire" , "expediteur_number", 'montant',DB::raw("(DATE_FORMAT(transactions.created_at, '%d-%m %H:%i:%s')) as mois"))
                ->get();
            return $transaction;
        } else {
            return null;
        }
        //return $this->getModel()->update($formRequest);
    }

    public function historyAdministrateurTotalEchec(User $user)
    {
        if ($user["users_type_type"] == "App\Models\Administrateur") {
            $transaction = Transaction::where('administrateur_id', $user["users_type_id"])
                ->where('statut', "Echec")
                ->limit(50)
                ->select('montant',"operateur_destinataire")
                ->get();
            return $transaction;
        } else {
            return null;
        }
        //return $this->getModel()->update($formRequest);
    }

    public function historyEntreprise(User $user)
    {
        if ($user["users_type_type"] == "App\Models\Administrateur") {

            $transaction = Transaction::select(
                DB::raw("(count(transactions.id)) as nombre_transaction"),
                DB::raw("(sum(transactions.commission)) as benefice"),
                DB::raw("(transactions.administrateur_id) as administrateur_id"),
                DB::raw("(DATE_FORMAT(transactions.created_at, '%m-%Y')) as mois"),
            )
            ->where('administrateur_id', $user["users_type_id"])
            ->where('statut', "Success")

                ->latest(DB::raw("DATE_FORMAT(transactions.created_at, '%m-%Y')"))
                ->limit(12)
                ->groupBy(
                    DB::raw("DATE_FORMAT(transactions.created_at, '%m-%Y')")
                )
                ->get();




            return $transaction;
        } else {
            return null;
        }



        //return $this->getModel()->update($formRequest);
    }

    public function currentBenefice(User $user)
    {
        if ($user["users_type_type"] == "App\Models\Administrateur") {

            $transaction = Transaction::select(
                DB::raw("(count(transactions.id)) as nombre_transaction"),
                DB::raw("(sum(transactions.commission)) as benefice"),
                DB::raw("(transactions.administrateur_id) as administrateur_id"),
                DB::raw("(DATE_FORMAT(transactions.created_at, '%m-%Y')) as mois"),
            )
            ->where('administrateur_id', $user["users_type_id"])
            ->where('statut', "Success")


                ->latest(DB::raw("DATE_FORMAT(transactions.created_at, '%m-%Y')"))

                ->groupBy(
                    DB::raw("DATE_FORMAT(transactions.created_at, '%m-%Y')")
                )
                ->first();





            return $transaction;
        } else {
            return null;
        }



        //return $this->getModel()->update($formRequest);
    }
}
