<?php
namespace App\Repositories\Implementation;
use App\Traits\ApiResponser;
use App\Http\Requests\administratorRequest;
use App\Models\Administrateur;
use App\Models\Administrator;
use App\Models\User;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use Illuminate\Http\Request;

class OperateurRepository extends GenericRepository
{
    use ApiResponser;

    public function model()
    {
        return 'App\Models\Operateur';
    }

    public function findOperateur(string $operateur)
    {
        $record = $this->getModel()->where('id', $operateur)->first();
        return $record;
    }







}
