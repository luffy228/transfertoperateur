<?php
namespace App\Repositories\Implementation;
use App\Traits\ApiResponser;
use App\Http\Requests\administratorRequest;
use App\Models\Administrateur;
use App\Models\Administrator;
use App\Models\User;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use Illuminate\Http\Request;

class SecretaireRepository extends GenericRepository
{
    use ApiResponser;

    public function model()
    {
        return 'App\Models\Secretaire';
    }

    public function register(Request $request)
    {
        $formRequest = [
            'administrateur_id'=> $request["admin"]
        ];
        return $this->getModel()->create($formRequest);
    }

    public function getAdmin()
    {

        //return $admin[0]['usersType']->with('country');
        $record = $this->getModel()
                    ->join('users','users.users_type_id','=','secretaires.id')
                    ->join('administrateurs','administrateurs.id','=','secretaires.administrateur_id')
                    ->where('users.users_type_type', "App\Models\Secretaire")
                    ->get();
        return $record;

    }



}
