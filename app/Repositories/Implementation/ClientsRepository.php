<?php

namespace App\Repositories\Implementation;


use App\Models\Clients;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Implementation\RoleRepository;
use App\Repositories\Implementation\CustomerRepository;
use App\Repositories\Generic\GenericImplementation\GenericRepository;

class ClientsRepository extends GenericRepository
{
    use ApiResponser;

    public function model()
    {
        return 'App\Models\Client';
    }

    public function addClient()
    {
        $formRequest = [
            'pays_id'=> 1
        ];
        return $this->getModel()->create($formRequest);

    }




    //login pour distributeur





}
