<?php

namespace App\Repositories\Generic\GenericInterface;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

interface GenericRepositoryInterface
{
    public function all();

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);

    public function find($id);

    public function findLibelle($name);

    public function findCountry($name);

    public function findName($name);

    public function login(Request $data);

    public function logout();

    public function restore(Model $model);

    public function updatePassword(Request $data);


}
