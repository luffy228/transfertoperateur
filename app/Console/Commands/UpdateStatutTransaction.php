<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateStatutTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateStatutTransaction:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = date('Y-m-d H:i',strtotime('-6 minutes'));

        $record = DB::table('transactions')

                    ->where('statut',"Attente")
                        ->where('created_at','<',$today)
                        ->update(["statut"=> "Echec"]);

                        $this->info('Successfully Mise a jour des statut en echec');
    }
}
