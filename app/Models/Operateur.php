<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operateur extends Model
{
    use HasFactory;

    protected $fillable = [
        'nom',
        'ussd_code',
        'pays_id'


    ];

    public function entrepriseNumber()
    {
        return $this->hasOne('App\Models\EntrepriseNumber');
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */

    /**
     * The attributes that should be cast.
     *
     * @var array
     */



    public function nameModel()
    {
        return 'Operateur';
    }
}
