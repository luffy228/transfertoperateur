<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\ClientsRepository;
use App\Repositories\Implementation\UserRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class ConnexionController extends Controller
{
    private $userRepo;
    private $clientRepo;
    use ApiResponser;


    public function __construct(UserRepository $userRepo , ClientsRepository $clientRepo)
    {
        $this->userRepo= $userRepo;
        $this->clientRepo= $clientRepo;
    }
    public function loginUser(Request $request)
    {
        $user = $this->userRepo->login($request);
        if ($user != null)
        {
            if ($user["connected"] == 0) {
                return $this->userRepo->loginUser($user);
            } else {
                return $this->errorResponse("vous etes deja connecter sur un autre appareil",400);
            }

        }else {
            return $this->errorResponse("numero de telephone ou mot de passe incorrect",400);
        }

    }

    public function logout()
    {

        $this->userRepo->logout();
        return $this->successResponse("deconnexion reussi");

    }

    public function registerUser(Request $request)
    {
        $client = $this->clientRepo->addClient($request);
        $role =   Role::find(2);
        $user = $this->userRepo->registerUser($request,$client->fresh() , $role);
        if ($user != null) {
            return $this->successResponse($user);
        }else{
            return $this->errorResponse("les informations saisies sont incorrects",400);
        }
    }

    public function verifyNumber(Request $request)
    {

        $user = $this->userRepo->verifyNumber($request["phone"]);
        return $this->successResponse($user);

    }
    /* Connected pour mettre a jour la colonne conneced d'un user pour determiner si il est connecter ou pas  */
    public function connected(Request $request)
    {
        $user = $this->userRepo->verifyNumber($request["phone"]);
        if ($user != null) {
            $user["connected"] = $request["connected"];
            $user->save();
            return $this->successResponse($user);
        }else{
            return $this->errorResponse("Numero inexistant dans la base de donnee",400);
        }

    }

    public function ForgetPassword(Request $request)
    {
        $user = $this->userRepo->verifyNumber($request["phone"]);
        if ($user != null) {
            $user["password"] = bcrypt($request["password"]);
            $user->save();
            return $this->successResponse($user);
        }else{
            return $this->errorResponse("Numero inexistant dans la base de donnee",400);
        }

    }
}
