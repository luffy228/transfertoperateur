<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\EntrepriseNumberRepository;
use App\Repositories\Implementation\OperateurRepository;
use App\Repositories\Implementation\TransactionRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Ably\AblyRest;
use App\Models\EntrepriseNumber;
use App\Repositories\Implementation\BeneficeRepository;
use App\Traits\Pourcentage;
use DateTime;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{

    private $entrepriseNumberRepo;
    private $operateurRepo;
    private $transactionRepo;
    private $beneficeRepo;
    use ApiResponser;
    use Pourcentage;



    public function __construct(EntrepriseNumberRepository $entrepriseNumberRepo ,BeneficeRepository $beneficeRepo, OperateurRepository $operateurRepo , TransactionRepository $transactionRepo)
    {
        $this->entrepriseNumberRepo = $entrepriseNumberRepo;
        $this->transactionRepo = $transactionRepo;
        $this->operateurRepo = $operateurRepo;
        $this->beneficeRepo = $beneficeRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operateur = $this->operateurRepo->findOperateur($request["operateur_destinataire"]);
        $user = auth()->guard('api')->user();
        $transaction = $this->transactionRepo->adding($request , $operateur , $user);
        return $this->successResponse($transaction,200);

      }

    public function countTransaction(Request $request)
    {
        //$operateur = $this->operateurRepo->findOperateur($request["operateur_destinataire"]);
        $user = auth()->guard('api')->user();
        $today = date("Y-m-d 00:00:00");
        $fin = date("Y-m-d 23:59:59");

        $nombre_transaction = $this->transactionRepo->countClientTransaction($today,$fin, $user);
        //var_dump($nombre_transaction);
        if ($nombre_transaction <= 20) {
            //$transaction = $this->transactionRepo->adding($request , $operateur , $user);
            return $this->successResponse("transaction",200);
        } else {
            return $this->errorResponse("Vous avez atteint vos 3 transactions autorisee par jour", 400);
        }

       //


    }

    public function beneficeSolux(Request $request)
    {
        //$operateur = $this->operateurRepo->findOperateur($request["operateur_destinataire"]);
        $user = auth()->guard('api')->user();
        if ($user["users_type_type"] == "App\Models\Administrateur") {
            $benef = $this->beneficeRepo->getBenefice( $user["users_type_id"]);
            $benefice = [];
             if ($benef == null) { $benefice = ["montant"=> "0"];
                return $this->successResponse($benefice,200);}
             else{
                 $benefice = ["montant"=> $benef["commission"]];
                return $this->successResponse($benefice,200);
            }


        }else{
            return $this->errorResponse("Vous n etes pas un administrateur", 400);
        }
        //
        //if ($nombre_transaction <= 20) {
            //$transaction = $this->transactionRepo->adding($request , $operateur , $user);
          //  return $this->successResponse("transaction",200);
        //} else {
          //
        //}

       //


    }

    public function findInformation(Request $request)
    {
        $data = [];
        $entreprise = $this->entrepriseNumberRepo->findNumberByOperatorExpediteur($request["operateur_expediteur"]);

        $montant = $request["montant"] + ( $request["montant"]/10) ;

        foreach ($entreprise as $admin) {
            $numero = $this->entrepriseNumberRepo->findNumberByOperator($request["operateur_destinataire"], $montant, $admin["administrateur_id"]);
            if ($numero != null) {
                array_push($data, $numero);
            }
        }


        // choisir un seul de facon aleatoire
        if (count($data) != 0) {
            $informationNumero["operateur"] = $this->entrepriseNumberRepo->getNumberByAdmin($data[0]->administrateur_id);
            $informationNumero["code"] = $this->operateurRepo->findOperateur($request["operateur_expediteur"]);
            return $this->successResponse($informationNumero);
        } else {
            $informationNumero["operateur"] =0;
            $informationNumero["code"] = $this->operateurRepo->findOperateur($request["operateur_expediteur"]);

            return $this->successResponse($informationNumero);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatut(Request $request)
    {
        $transaction = $this->transactionRepo->updateStatut($request);
        if ($request["statut"] == "Success") {
            $entreprise = $this->entrepriseNumberRepo->getEntrepriseSolde($transaction["administrateur_id"] , $transaction["operateur_expediteur"]);
            $entreprise["solde"] =$entreprise["solde"] -  $transaction["montant"];
            $entreprise->save();
            $entrepriseDest = DB::table('entreprise_numbers')  ->where('administrateur_id',$transaction["administrateur_id"])
            ->where('operateur_id', $transaction["operateur_destinataire"])
            ->first();
            $montant =$entrepriseDest->solde +  $transaction["montant"];
            $entrepriseDest = DB::table('entreprise_numbers')  ->where('administrateur_id',$transaction["administrateur_id"])
            ->where('operateur_id', $transaction["operateur_destinataire"])
            ->update(['solde'=>$montant ]);
            $benef = $this->beneficeRepo->getBenefice($transaction["administrateur_id"]);
            if ($benef == null) {
                if ($transaction["administrateur_id"] == 1) {
                    $commission = $transaction["commission"];
                } else {
                    $commission = $transaction["commission"]*$this->pourcentage();
                }

                $this->beneficeRepo->adding($commission ,$transaction["administrateur_id"] );



            }else {
                if ($transaction["administrateur_id"] == 1) {
                    $benef["commission"] = $transaction["commission"] +  $benef["commission"];
                } else {
                    $benef["commission"] = ($transaction["commission"]*$this->pourcentage())+ $benef["commission"];
                }
                $benef->save();
            }

        }
        // Mettre a jour les soldes dans une file
        return $this->successResponse($transaction, 200);
    }



    public function historyClient()
    {
        //
        $user = auth()->guard('api')->user();

        $transaction = $this->transactionRepo->historyClient($user);

        // Mettre a jour les soldes dans une file
        if ($transaction != null) {
            return $this->successResponse($transaction, 200);
        } else {
            return $this->errorResponse("Vous n'avez pas droit a cette page", 400);
        }
    }

    public function historySummarize()
    {
        //
        $user = auth()->guard('api')->user();
        $transaction = $this->transactionRepo->historyEntreprise($user);
        // Mettre a jour les soldes dans une file
        if ($transaction != null) {
            return $this->successResponse($transaction, 200);
        } else {
            return $this->errorResponse("Vous n'avez pas droit a cette page", 400);
        }
    }

    public function historyEntreprise()
    {
        //
        $user = auth()->guard('api')->user();
        $transaction = $this->transactionRepo->historyAdministrateur($user);
        // Mettre a jour les soldes dans une file
        if ($transaction != null) {
            return $this->successResponse($transaction, 200);
        } else {
            return $this->errorResponse("Vous n'avez pas droit a cette page", 400);
        }
    }

    public function historyEntrepriseEchec()
    {
        //
        $user = auth()->guard('api')->user();
        $transaction = $this->transactionRepo->historyAdministrateurEchec($user);
        // Mettre a jour les soldes dans une file
        if ($transaction != null) {
            return $this->successResponse($transaction, 200);
        } else {
            return $this->errorResponse("Vous n'avez pas droit a cette page", 400);
        }
    }

    public function EntrepriseMontantResend()
    {
        //
        $user = auth()->guard('api')->user();
        $transaction = $this->transactionRepo->historyAdministrateurTotalEchec($user);
        $sommeFlooz = 0;
        $sommeTmoney = 0;
        // Mettre a jour les soldes dans une file
        if ($transaction != null) {
            foreach ($transaction as $total) {
                if ($total["operateur_destinataire"] == 1) {
        $sommeTmoney = $sommeTmoney+$total["montant"] ;

                }

                if ($total["operateur_destinataire"] == 2) {
                    $sommeFlooz = $sommeFlooz+$total["montant"] ;

                }
            }
            $data = ["tmoney"=> $sommeTmoney ,"flooz"=> $sommeFlooz ];
            return $this->successResponse($data, 200);
        } else {
            return $this->errorResponse("Vous n'avez pas droit a cette page", 400);
        }
    }

    public function EntrepriseBeneficeUpdate()
    {
        //
        $user = auth()->guard('api')->user();
        $transaction = $this->transactionRepo->currentBenefice($user);
        return $this->successResponse($transaction, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
