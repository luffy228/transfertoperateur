<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Administrateur;
use App\Repositories\Implementation\EntrepriseNumberRepository;
use App\Repositories\Implementation\OperateurRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EntrepriseNumberController extends Controller
{
    private $userRepo;
    private $operateurRepo;
    private $entrepriseNumberRepo;



    public function __construct(UserRepository $userRepo , OperateurRepository $operateurRepo , EntrepriseNumberRepository $entrepriseNumberRepo)
    {
        $this->userRepo= $userRepo;
        $this->operateurRepo= $operateurRepo;
        $this->entrepriseNumberRepo= $entrepriseNumberRepo;
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $operateur = $this->operateurRepo->all();
        $user = Auth::user();
        $number = $this->entrepriseNumberRepo->getNumberBySociete($user);

        return view('template.backend.Listnumero' , compact('operateur', 'number'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = Auth::user();
        $numero = $this->entrepriseNumberRepo->adding($request, $user);
        return redirect()->to('admin/listnumero');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
