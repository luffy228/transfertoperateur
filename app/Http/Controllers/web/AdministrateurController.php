<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AdministratorRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class AdministrateurController extends Controller
{

    private $userRepo;
    private $administrateurRepo;


    public function __construct(UserRepository $userRepo , AdministratorRepository $administrateurRepo)
    {
        $this->userRepo= $userRepo;
        $this->administrateurRepo= $administrateurRepo;
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $admin = $this->administrateurRepo->register($request);
        $role =   Role::find(3);
        $user = $this->userRepo->registerUser($request,$admin->fresh() , $role);
        if ($user != null) {
                return redirect()->to('admin/listsociete');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
