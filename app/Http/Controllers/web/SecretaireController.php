<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AdministratorRepository;
use App\Repositories\Implementation\SecretaireRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class SecretaireController extends Controller
{
    private $userRepo;
    private $secretaireRepo;
    private $administrateurRepo;


    public function __construct(UserRepository $userRepo , SecretaireRepository $secretaireRepo , AdministratorRepository $administrateurRepo)
    {
        $this->userRepo= $userRepo;
        $this->secretaireRepo= $secretaireRepo;
        $this->administrateurRepo= $administrateurRepo;
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $admin = $this->secretaireRepo->getAdmin();
        //dd($admin);
        $societe = $this->administrateurRepo->all();
        //dd($societe);

        return view('template.backend.Listsecretaire' , compact('societe', 'admin'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // nom de la societe
        $admin = $this->secretaireRepo->register($request);
        $role =   Role::find(4);
        $user = $this->userRepo->registerUser($request,$admin->fresh() , $role);
        if ($user != null) {
                return redirect()->to('admin/listsecretaire');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
