<?php

use App\Http\Controllers\api\ConnexionController;
use App\Http\Controllers\api\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  //  return $request->user();
//});

Route::post('/login', [ConnexionController::class,'loginUser']);
Route::post('/register', [ConnexionController::class,'registerUser']);
Route::post('/createTransaction', [TransactionController::class,'store']);
Route::get('/countUserTransaction', [TransactionController::class,'countTransaction']);
Route::get('/logout', [ConnexionController::class,'logout']);
Route::get('/beneficeSolux', [TransactionController::class,'beneficeSolux']);
Route::post('/findEntrepriseNumber', [TransactionController::class,'findInformation']);
Route::post('/updateStatutTransaction', [TransactionController::class,'updateStatut']);
Route::post('/verifyNumber', [ConnexionController::class,'verifyNumber']);
Route::post('/connected', [ConnexionController::class,'connected']);
Route::get('/ClientHistoryTransaction', [TransactionController::class,'historyClient']);
Route::get('/EntrepriseHistorySummarize', [TransactionController::class,'historySummarize']);
Route::get('/EntrepriseHistoryTransaction', [TransactionController::class,'historyEntreprise']);
Route::get('/EntrepriseHistoryTransactionEchec', [TransactionController::class,'historyEntrepriseEchec']);
Route::get('/EntrepriseOperateurResend', [TransactionController::class,'EntrepriseMontantResend']);
Route::get('/EntrepriseUpdateBenefice', [TransactionController::class,'EntrepriseBeneficeUpdate']);
Route::post('/ForgetPassword', [ConnexionController::class,'forgetPassword']);
//ssRoute::post('/EntreprisePayement', [TransactionController::class,'Payement]);

// Payer le commission de e-solux
