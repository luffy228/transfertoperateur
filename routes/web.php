<?php

use App\Http\Controllers\web\AdministrateurController;
use App\Http\Controllers\web\ConnexionController;
use App\Http\Controllers\web\EntrepriseNumberController;
use App\Http\Controllers\web\HomeController;
use App\Http\Controllers\web\ProfileController;
use App\Http\Controllers\web\SecretaireController;
use App\Http\Controllers\web\SocieteController;
use App\Http\Controllers\web\VirementController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login');

Route::prefix("/login")->group(function () {
    Route::post('/login', [ConnexionController::class,'loginAdmin'])->name('connexion');
});

Route::prefix("/admin")->group(function () {
    Route::get('/home', [HomeController::class,'index'])->name('home');
    Route::get('/listsociete', [SocieteController::class,'index'])->name('societeall');
    Route::get('/listsecretaire', [SecretaireController::class,'index'])->name('secretaireall');
    Route::get('/listvirement', [VirementController::class,'index'])->name('virementesolux');
    Route::get('/listvirementwaiting', [VirementController::class,'indexwaiting'])->name('virementesoluxwaiting');
    Route::get('/listvirementsociete', [VirementController::class,'indexsociete'])->name('virementsociete');
    Route::get('/listnumero', [EntrepriseNumberController::class,'index'])->name('numeroall');
    Route::get('/profile', [ProfileController::class,'index'])->name('getprofile');
    Route::get('/logout', [ConnexionController::class,'logout'])->name('logout');


    Route::post('/addingsociete',[AdministrateurController::class,'store'])->name('storesociete');
    Route::post('/addingnumber',[EntrepriseNumberController::class,'store'])->name('storenumber');
    Route::post('/addingsecretaire',[SecretaireController::class,'store'])->name('storesecretaire');










});










